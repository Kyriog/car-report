/*
 * Copyright 2013 Jan Kühle
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package me.kuehle.carreport.gui;

import android.app.ListFragment;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import me.kuehle.carreport.Application;
import me.kuehle.carreport.R;
import me.kuehle.carreport.db.Car;
import me.kuehle.carreport.gui.dialog.MessageDialogFragment;
import me.kuehle.carreport.gui.dialog.MessageDialogFragment.MessageDialogFragmentListener;

public class PreferencesCarsFragment extends ListFragment implements
        MessageDialogFragmentListener {
    private class CarAdapter extends BaseAdapter {
        private List<Car> mCars;

        public CarAdapter() {
            mCars = Car.getAll();
        }

        @Override
        public int getCount() {
            return mCars.size();
        }

        @Override
        public Car getItem(int position) {
            return mCars.get(position);
        }

        @Override
        public long getItemId(int position) {
            return getItem(position).id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            CarViewHolder holder;
            if (convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(
                        R.layout.list_item_car, parent, false);

                holder = new CarViewHolder();
                holder.name = (TextView) convertView
                        .findViewById(android.R.id.text1);
                holder.suspended = (TextView) convertView
                        .findViewById(android.R.id.text2);
                holder.color = convertView.findViewById(android.R.id.custom);

                convertView.setTag(holder);
            } else {
                holder = (CarViewHolder) convertView
                        .getTag();
            }

            holder.name.setText(mCars.get(position).name);
            if (mCars.get(position).isSuspended()) {
                holder.suspended.setText(getString(
                        R.string.suspended_since,
                        android.text.format.DateFormat.getDateFormat(
                                getActivity()).format(
                                mCars.get(position).suspendedSince)));
                holder.suspended.setVisibility(View.VISIBLE);
            } else {
                holder.suspended.setVisibility(View.GONE);
            }

            holder.color.getBackground().setColorFilter(mCars.get(position).color,
                    PorterDuff.Mode.SRC);
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        public void update() {
            mCars = Car.getAll();
            notifyDataSetChanged();
        }
    }

    private static class CarViewHolder {
        public TextView name;
        public TextView suspended;
        public View color;
    }

    private class CarMultiChoiceModeListener implements MultiChoiceModeListener {
        private ActionMode mActionMode;

        public void finishActionMode() {
            if (mActionMode != null) {
                mActionMode.finish();
            }
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menu_delete:
                    if (getListView().getCheckedItemCount() == mCarAdapter.getCount()) {
                        MessageDialogFragment.newInstance(null, 0,
                                R.string.alert_delete_title,
                                getString(R.string.alert_cannot_delete_last_car),
                                android.R.string.ok, null).show(
                                getFragmentManager(), null);
                    } else {
                        String message = getString(R.string.alert_delete_cars_message,
                                getListView().getCheckedItemCount());
                        MessageDialogFragment.newInstance(
                                PreferencesCarsFragment.this, DELETE_REQUEST_CODE,
                                R.string.alert_delete_title, message,
                                android.R.string.yes, android.R.string.no).show(
                                getFragmentManager(), null);
                    }
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mActionMode = mode;
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.edit_cars_cab, menu);
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id,
                                              boolean checked) {
            int count = getListView().getCheckedItemCount();
            mode.setTitle(String.format(getString(R.string.cab_title_selected), count));
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
    }

    private static final int DELETE_REQUEST_CODE = 1;

    private CarAdapter mCarAdapter;
    private CarMultiChoiceModeListener mMultiChoiceModeListener;
    private boolean mCarEditInProgress = false;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCarAdapter = new CarAdapter();
        mMultiChoiceModeListener = new CarMultiChoiceModeListener();

        getListView().setMultiChoiceModeListener(mMultiChoiceModeListener);
        getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        setListAdapter(mCarAdapter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_cars, menu);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        openCarDetailFragment(id);
    }

    @Override
    public void onDialogNegativeClick(int requestCode) {
    }

    @Override
    public void onDialogPositiveClick(int requestCode) {
        if (requestCode == DELETE_REQUEST_CODE) {
            long[] checkedIds = getListView().getCheckedItemIds();
            for (long id : checkedIds) {
                Car.delete(Car.class, id);
            }

            Application.dataChanged();

            mMultiChoiceModeListener.finishActionMode();
            mCarAdapter.update();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_add_car:
                openCarDetailFragment(AbstractDataDetailFragment.EXTRA_ID_DEFAULT);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCarEditInProgress) {
            mCarEditInProgress = false;
            mCarAdapter.update();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        mMultiChoiceModeListener.finishActionMode();
    }

    private void openCarDetailFragment(long id) {
        Intent intent = new Intent(getActivity(), DataDetailActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.putExtra(DataDetailActivity.EXTRA_EDIT, DataDetailActivity.EXTRA_EDIT_CAR);
        intent.putExtra(AbstractDataDetailFragment.EXTRA_ID, id);
        startActivityForResult(intent, 0);

        mCarEditInProgress = true;
    }
}